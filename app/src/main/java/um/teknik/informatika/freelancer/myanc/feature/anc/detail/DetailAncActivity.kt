package um.teknik.informatika.freelancer.myanc.feature.anc.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_detail_anc.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.util.DateUtils

class DetailAncActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_ANC = "extra_kunjungan_anc"
        fun getIntent(context: Context, anc: Kunjungan): Intent {
            val intent = Intent(context, DetailAncActivity::class.java)
            intent.putExtra(EXTRA_ANC, anc)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_anc)

        val anc = intent.getParcelableExtra<Kunjungan>(EXTRA_ANC)
        if (anc != null) fetchData(anc)
        else onGetDataFailed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun fetchData(anc: Kunjungan) {
        text_tanggal.text = getString(
                R.string.detail_hint_tanggal,
                DateUtils.convertDate(anc.tanggal)
        )
        text_kehamilan.text = getString(R.string.detail_hint_kehamilan, anc.umurKehamilan)
        text_darah.text = getString(R.string.detail_hint_darah, anc.tekananDarah)
        text_berat.text = getString(R.string.detail_hint_berat, anc.beratBadan)
        text_tinggi.text = getString(R.string.detail_hint_tinggi, anc.tinggiFundus)
        text_janin.text = getString(R.string.detail_hint_janin, anc.letakJanin)
        text_jantung.text = getString(R.string.detail_hint_jantung, anc.denyutJanin)
        text_kaki.text = getString(
                R.string.detail_hint_kaki,
                anc.kakiBengkak
        )
        text_kaki.setCompoundDrawablesRelativeWithIntrinsicBounds(
                if (anc.kakiBengkak == "Ya") R.drawable.ic_yes_accent_24dp
                else R.drawable.ic_no_accent_24dp,
                0, 0, 0
        )
        text_kembali.text = DateUtils.convertDate(anc.tanggalKembali)

        text_keluhan.text = anc.keluhan
        text_hasil.text = anc.hasilLab
        text_tindakan.text = anc.tindakan
        text_nasehat.text = anc.nasehat
        text_keterangan.text = anc.keterangan
    }

    private fun onGetDataFailed() {
        Toast.makeText(this, R.string.general_error_no_internet, Toast.LENGTH_LONG).show()
    }
}
