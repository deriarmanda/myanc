package um.teknik.informatika.freelancer.myanc.feature.home

import android.net.Uri
import android.util.Log
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.base.BaseRepositoryCallback
import um.teknik.informatika.freelancer.myanc.data.model.Identitas
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.User
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.IdentitasRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KehamilanRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository

class HomePresenter (
        private val view: HomeContract.View,
        private val accountRepo: AccountRepository,
        private val identitasRepo: IdentitasRepository,
        private val kehamilanRepo: KehamilanRepository,
        private val kunjunganRepo: KunjunganRepository
) : HomeContract.Presenter {

    private var mIdentitas: Identitas? = null
    private var mDetailAnc: Kunjungan? = null

    override fun loadAkunUser() {
        view.setLoadingIndicator(true)
        val user = accountRepo.getCurrentUser()
        view.onLoadAkunUserSucceed(user)
        loadIdentitasUser(user)
    }

    override fun loadIdentitasUser(user: User) {
        view.setLoadingIndicator(true)
        identitasRepo.getIdentitas(
                user.uid,
                object : IdentitasRepository.Callback {
                    override fun onGetIdentitasSucceed(identitas: Identitas) {
                        mIdentitas = identitas
                        view.onLoadIdentitasUserSucceed(identitas)
                        loadCatatanTerkini(user)
                    }

                    override fun onGetIdentitasFailed(message: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadAllDataFailed(R.string.general_error_no_internet)
                        Log.d("HomePresenter", message)
                    }
                }
        )
    }

    override fun loadCatatanTerkini(user: User) {
        view.setLoadingIndicator(true)
        kehamilanRepo.getCurrentKehamilan(
                user.uid,
                object : KehamilanRepository.Callback {
                    override fun onGetCurrentKehamilanSuccess(kehamilan: Kehamilan) {
                        loadCurrentAnc(kehamilan)
                    }

                    override fun onLoadListKehamilanSuccess(list: List<Kehamilan>) {}
                    override fun onTaskFailed(message: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadAllDataFailed(R.string.general_error_no_internet)
                        Log.d("HomePresenter", message)
                    }
                }
        )
    }

    override fun doSignOut() {
        view.setLoadingIndicator(true)
        accountRepo.logoutUser(object : BaseRepositoryCallback {
            override fun onRepositoryTaskSuccess() {
                view.setLoadingIndicator(false)
                view.onSignedOut()
            }

            override fun onRepositoryTaskFailed(message: String) { }
        })
    }

    override fun callPetugas() {
        view.showDialerPage(Uri.parse("tel: ${mIdentitas?.telpPetugas}"))
    }

    override fun openIdentitasPasienPage() {
        view.showIdentitasPasienPage(mIdentitas!!)
    }

    override fun openDetailAncPage() {
        view.showDetailAncPage(mDetailAnc!!)
    }

    override fun start() = loadAkunUser()

    private fun loadCurrentAnc(kehamilan: Kehamilan) {
        view.setLoadingIndicator(true)
        kunjunganRepo.getCurrentKunjungan(
                kehamilan.uidAnc,
                object : KunjunganRepository.Callback {
                    override fun onGetCurrentKunjunganSuccess(anc: Kunjungan) {
                        view.setLoadingIndicator(false)
                        view.onLoadCatatanTerkiniSucceed(anc)
                        mDetailAnc = anc
                    }

                    override fun onGetListKunjunganSuccess(list: List<Kunjungan>) {}
                    override fun onTaskFailed(message: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadAllDataFailed(R.string.general_error_no_internet)
                        Log.d("HomePresenter", message)
                    }
                }
        )
    }
}