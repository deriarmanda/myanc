package um.teknik.informatika.freelancer.myanc.data.model

import android.os.Parcel
import android.os.Parcelable

data class Kehamilan(
        val kehamilanKe: Int = 1,
        val tanggalAwal: String = "",
        val uidAnc: String = "",
        val uidSkrining: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(kehamilanKe)
        parcel.writeString(tanggalAwal)
        parcel.writeString(uidAnc)
        parcel.writeString(uidSkrining)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Kehamilan> {
        override fun createFromParcel(parcel: Parcel): Kehamilan {
            return Kehamilan(parcel)
        }

        override fun newArray(size: Int): Array<Kehamilan?> {
            return arrayOfNulls(size)
        }
    }
}