package um.teknik.informatika.freelancer.myanc.feature.catatan

import android.util.Log
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.AntenatalCare
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.Skrining
import um.teknik.informatika.freelancer.myanc.data.repository.CatatanRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository

class CatatanKesehatanPresenter(
        private val mView: CatatanKesehatanContract.View,
        private val mKehamilan: Kehamilan,
        private val mCatatanRepo: CatatanRepository,
        private val mKunjunganRepo: KunjunganRepository
) : CatatanKesehatanContract.Presenter {

    override fun loadStaticAnc() {
        mView.setLoadingIndicator(true)
        val uidAnc = mKehamilan.uidAnc

        mCatatanRepo.getAnc(
                uidAnc,
                object : CatatanRepository.Callback {
                    override fun onGetAncSuccess(anc: AntenatalCare) {
                        mView.showStaticAnc(anc)
                        loadListSkrining()
                    }

                    override fun onGetSkriningSuccess(list: List<Skrining>) {}
                    override fun onTaskFailed(message: String) {
                        handleError(message)
                    }
                }
        )
    }

    override fun loadListSkrining() {
        mView.setLoadingIndicator(true)
        val uidSkrining = mKehamilan.uidSkrining

        mCatatanRepo.getSkrining(
                uidSkrining,
                object : CatatanRepository.Callback {
                    override fun onGetAncSuccess(anc: AntenatalCare) {}
                    override fun onGetSkriningSuccess(list: List<Skrining>) {
                        mView.showListSkrining(list)
                        loadKunjunganAnc(6)
                    }

                    override fun onTaskFailed(message: String) {
                        handleError(message)
                    }
                }
        )
    }

    override fun loadKunjunganAnc(limit: Int) {
        mView.setLoadingIndicator(true)
        val uidAnc = mKehamilan.uidAnc

        mKunjunganRepo.getListKunjungan(
                uidAnc,
                object : KunjunganRepository.Callback {
                    override fun onGetCurrentKunjunganSuccess(anc: Kunjungan) {}
                    override fun onGetListKunjunganSuccess(list: List<Kunjungan>) {
                        mView.setLoadingIndicator(false)
                        mView.showListKunjunganAnc(list)
                    }

                    override fun onTaskFailed(message: String) {
                        handleError(message)
                    }
                }
        )
    }

    override fun start() = loadStaticAnc()

    private fun handleError(message: String) {
        mView.setLoadingIndicator(false)
        mView.onLoadDataFailed(R.string.general_error_no_internet)
        Log.d("CatatanPresenter", message)
    }
}