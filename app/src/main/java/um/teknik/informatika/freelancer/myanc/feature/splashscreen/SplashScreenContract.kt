package um.teknik.informatika.freelancer.myanc.feature.splashscreen

import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView

interface SplashScreenContract {

    interface Presenter : BasePresenter {
        fun checkAnonymousFirebaseUser()
        fun checkLogedInUser()
        fun loginAnonymouslyToFirebase()
    }

    interface View : BaseView<Presenter> {
        fun onUserAlreadyLogin()
        fun onAnonymousLoginSuccess()
        fun onAnonymousLoginFailed(@StringRes msgRes: Int)
    }
}