package um.teknik.informatika.freelancer.myanc.data.model

data class User(
        val uid: String = "",
        val noRM: String = "",
        val password: String = ""
)