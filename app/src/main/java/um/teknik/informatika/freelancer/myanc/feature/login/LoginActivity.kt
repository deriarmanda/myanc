package um.teknik.informatika.freelancer.myanc.feature.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.feature.home.HomeActivity

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override lateinit var presenter: LoginContract.Presenter

    companion object {
        fun getIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this, AccountRepository.getInstance(this))

        button_login.setOnClickListener {
            presenter.doLogin(
                    form_norm.text.toString(),
                    form_password.text.toString()
            )
        }

    }

    override fun onAuthenticationSuccess() {
        Toast.makeText(this, R.string.login_msg_auth_success, Toast.LENGTH_LONG).show()
        startActivity(HomeActivity.getIntent(this))
        finish()
    }

    override fun onAuthenticationFailed(msgRes: Int) {
        Snackbar.make(root, msgRes, Snackbar.LENGTH_LONG).show()
    }

    override fun onNetworkErrorOccured() {
        Snackbar.make(root, R.string.general_error_no_internet, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.general_action_retry) {
                    presenter.doLogin(
                            form_norm.text.toString(),
                            form_password.text.toString()
                    )
                }.show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }
}
