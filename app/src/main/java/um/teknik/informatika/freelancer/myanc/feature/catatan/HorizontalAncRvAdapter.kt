package um.teknik.informatika.freelancer.myanc.feature.catatan

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_anc_horizontal.view.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.util.DateUtils

class HorizontalAncRvAdapter(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<HorizontalAncRvAdapter.ViewHolder>() {

    var listAnc = emptyList<Kunjungan>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_anc_horizontal, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = listAnc.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(listAnc[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(anc: Kunjungan) {
            itemView.text_tanggal.text = DateUtils.convertDate(anc.tanggal)
            itemView.text_kehamilan.text = String.format("Umur Kehamilan: %s", anc.umurKehamilan)
            itemView.text_others.text = String.format("%s - %s", anc.tekananDarah, anc.beratBadan)
            itemView.container.setOnClickListener { mClickListener.onItemClick(anc) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(anc: Kunjungan)
    }
}