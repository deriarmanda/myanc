package um.teknik.informatika.freelancer.myanc.feature.kehamilan

import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan

interface ListKehamilanContract {

    interface Presenter : BasePresenter {
        fun loadListKehamilan()
    }

    interface View : BaseView<Presenter> {
        fun onLoadListKehamilanSucceed(list: List<Kehamilan>)
        fun onLoadListKehamilanFailed(msgRes: Int)
        fun showEmptyListMessage()
    }
}