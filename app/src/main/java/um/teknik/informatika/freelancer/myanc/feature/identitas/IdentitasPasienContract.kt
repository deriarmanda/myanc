package um.teknik.informatika.freelancer.myanc.feature.identitas

import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView
import um.teknik.informatika.freelancer.myanc.data.model.Identitas

interface IdentitasPasienContract {

    interface Presenter : BasePresenter {
        fun reloadIdentitas()
    }

    interface View : BaseView<Presenter> {
        fun showIdentitas(identitas: Identitas)
        fun showErrorDialog(@StringRes msgRes: Int)
    }
}