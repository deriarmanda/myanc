package um.teknik.informatika.freelancer.myanc.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan

class KunjunganRepository private constructor() {

    private val mDbReference =
            FirebaseDatabase.getInstance().getReference("/anc_2")

    companion object {
        private var sInstance: KunjunganRepository? = null
        fun getInstance(): KunjunganRepository {
            if (sInstance == null) sInstance = KunjunganRepository()
            return sInstance!!
        }
    }

    fun getListKunjungan(uidAnc: String, callback: Callback, limit: Int = -1) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val list = arrayListOf<Kunjungan>()
                    for (child in data.children) {
                        val anc = child.getValue(Kunjungan::class.java)
                        if (anc == null) callback.onTaskFailed("NPE occured while trying to parse List ANC value from Firebase")
                        else list.add(anc)
                    }
                    callback.onGetListKunjunganSuccess(list)
                } else callback.onGetListKunjunganSuccess(emptyList())
            }
        }

        val dbRef = mDbReference.child(uidAnc)
                .orderByChild("tanggal")
        if (limit != -1) dbRef.limitToLast(limit)
        dbRef.addListenerForSingleValueEvent(listener)
    }

    fun getCurrentKunjungan(uidAnc: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    for (child in data.children) {
                        val anc = child.getValue(Kunjungan::class.java)
                        if (anc != null) callback.onGetCurrentKunjunganSuccess(anc)
                        else callback.onTaskFailed("NPE occured while trying to parse Detail ANC value from Firebase")
                        break
                    }
                } else callback.onGetCurrentKunjunganSuccess(Kunjungan())
            }
        }

        val dbRef = mDbReference.child(uidAnc)
                .orderByChild("tanggal")
                .limitToLast(1)
        dbRef.addListenerForSingleValueEvent(listener)
    }

    interface Callback {
        fun onGetCurrentKunjunganSuccess(anc: Kunjungan)
        fun onGetListKunjunganSuccess(list: List<Kunjungan>)
        fun onTaskFailed(message: String)
    }
}