package um.teknik.informatika.freelancer.myanc.feature.kehamilan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_list_kehamilan.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KehamilanRepository
import um.teknik.informatika.freelancer.myanc.feature.catatan.CatatanKesehatanActivity

class ListKehamilanActivity : AppCompatActivity(), ListKehamilanContract.View {

    override lateinit var presenter: ListKehamilanContract.Presenter
    private lateinit var snackbar: Snackbar
    private val itemClickListener =
            object : ListKehamilanRvAdapter.OnItemClickListener {
                override fun onItemClick(kehamilan: Kehamilan) {
                    startActivity(CatatanKesehatanActivity
                            .getIntent(this@ListKehamilanActivity, kehamilan))
                }
            }
    private val rvAdapter = ListKehamilanRvAdapter(itemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, ListKehamilanActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_kehamilan)

        presenter = ListKehamilanPresenter(
                this,
                KehamilanRepository.getInstance(),
                AccountRepository.getInstance(this)
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                snackbar.dismiss()
                presenter.loadListKehamilan()
            }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@ListKehamilanActivity)
            adapter = rvAdapter
        }
        snackbar = Snackbar.make(
                refresh_layout,
                R.string.general_error_no_internet,
                Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.general_action_retry) {
            presenter.loadListKehamilan()
        }

        presenter.start()
    }

    override fun onLoadListKehamilanSucceed(list: List<Kehamilan>) {
        text_empty_msg.visibility = View.GONE
        recycler_view.visibility = View.VISIBLE
        rvAdapter.listKehamilan = list
    }

    override fun onLoadListKehamilanFailed(msgRes: Int) {
        text_empty_msg.visibility = View.GONE
        recycler_view.visibility = View.GONE
        snackbar.setText(msgRes).show()
    }

    override fun showEmptyListMessage() {
        text_empty_msg.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
