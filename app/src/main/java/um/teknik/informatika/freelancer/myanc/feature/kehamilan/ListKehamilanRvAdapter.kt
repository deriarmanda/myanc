package um.teknik.informatika.freelancer.myanc.feature.kehamilan

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_kehamilan.view.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.util.DateUtils

class ListKehamilanRvAdapter(
        private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ListKehamilanRvAdapter.ViewHolder>() {

    var listKehamilan = listOf<Kehamilan>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_kehamilan, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(listKehamilan[position])
    }

    override fun getItemCount(): Int = listKehamilan.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(kehamilan: Kehamilan) {
            itemView.text_kehamilan.text = String.format(
                    "Kehamilan ke - %d",
                    kehamilan.kehamilanKe
            )
            itemView.text_kunjungan_awal.text = String.format(
                    "%s (Kunjungan Awal)",
                    DateUtils.convertDate(kehamilan.tanggalAwal)
            )
            itemView.button_show_data.setOnClickListener {
                itemClickListener.onItemClick(kehamilan)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(kehamilan: Kehamilan)
    }
}