package um.teknik.informatika.freelancer.myanc.feature.login

import android.util.Log
import um.teknik.informatika.freelancer.myanc.data.model.User
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository

class LoginPresenter (
        private val view: LoginContract.View,
        private val accountRepo: AccountRepository
) : LoginContract.Presenter {

    private val accountCallback = object : AccountRepository.Callback {
        override fun onAuthenticateSuccess() {
            view.setLoadingIndicator(false)
            view.onAuthenticationSuccess()
        }

        override fun onAuthenticateFailed(msgRes: Int) {
            view.setLoadingIndicator(false)
            view.onAuthenticationFailed(msgRes)
        }

        override fun onRepositoryTaskSuccess() { }
        override fun onRepositoryTaskFailed(message: String) {
            view.setLoadingIndicator(false)
            view.onNetworkErrorOccured()
            Log.d("LoginPresenter", message)
        }
    }

    override fun doLogin(noRM: String, password: String) {
        view.setLoadingIndicator(true)
        accountRepo.authenticate(noRM, password, accountCallback)
    }

    override fun start() { }
}