package um.teknik.informatika.freelancer.myanc.data.model

import android.os.Parcel
import android.os.Parcelable

data class Kunjungan(
        val tanggal: String = "",
        val tanggalKembali: String = "",
        val umurKehamilan: String = "",
        val tekananDarah: String = "",
        val beratBadan: String = "",
        val tinggiFundus: String = "",
        val letakJanin: String = "",
        val denyutJanin: String = "",
        val kakiBengkak: String = "",
        val keluhan: String = "",
        val hasilLab: String = "",
        val tindakan: String = "",
        val nasehat: String = "",
        val keterangan: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tanggal)
        parcel.writeString(tanggalKembali)
        parcel.writeString(umurKehamilan)
        parcel.writeString(tekananDarah)
        parcel.writeString(beratBadan)
        parcel.writeString(tinggiFundus)
        parcel.writeString(letakJanin)
        parcel.writeString(denyutJanin)
        parcel.writeString(kakiBengkak)
        parcel.writeString(keluhan)
        parcel.writeString(hasilLab)
        parcel.writeString(tindakan)
        parcel.writeString(nasehat)
        parcel.writeString(keterangan)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Kunjungan> {
        override fun createFromParcel(parcel: Parcel): Kunjungan {
            return Kunjungan(parcel)
        }

        override fun newArray(size: Int): Array<Kunjungan?> {
            return arrayOfNulls(size)
        }
    }
}