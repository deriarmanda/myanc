package um.teknik.informatika.freelancer.myanc.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan

class KehamilanRepository private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance().getReference("/kehamilan")

    companion object {
        private var sInstance: KehamilanRepository? = null
        fun getInstance(): KehamilanRepository {
            if (sInstance == null) sInstance = KehamilanRepository()
            return sInstance!!
        }
    }

    fun getListKehamilan(uidUser: String, callback: Callback) {
        /*callback.onLoadListKehamilanSucceed(
                listOf(
                        Kehamilan(2, "29 Desember 2019", "uidANC", "uidSkrining"),
                        Kehamilan(1, "10 November 2018", "uidANC", "uidSkrining")
                )
        )*/
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val list = arrayListOf<Kehamilan>()
                    for (child in data.children) {
                        val kehamilan = child.getValue(Kehamilan::class.java)
                        if (kehamilan != null) list.add(kehamilan)
                        else callback.onTaskFailed("NPE Occured while trying to parse Kehamilan value")
                    }
                    callback.onLoadListKehamilanSuccess(list)
                } else callback.onLoadListKehamilanSuccess(emptyList())
            }
        }

        val userRef = mDatabase.child(uidUser)
        userRef.addListenerForSingleValueEvent(listener)
    }

    fun getCurrentKehamilan(uidUser: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    for (child in data.children) {
                        val kehamilan = child.getValue(Kehamilan::class.java)
                        if (kehamilan != null) callback.onGetCurrentKehamilanSuccess(kehamilan)
                        else callback.onTaskFailed("NPE occured while trying to parse Current Kehamilan value from Firebase")
                        break
                    }
                } else callback.onGetCurrentKehamilanSuccess(Kehamilan())
            }
        }

        val dbRef = mDatabase.child(uidUser)
                .orderByChild("kehamilanKe")
                .limitToLast(1)
        dbRef.addListenerForSingleValueEvent(listener)
    }

    interface Callback {
        fun onGetCurrentKehamilanSuccess(kehamilan: Kehamilan)
        fun onLoadListKehamilanSuccess(list: List<Kehamilan>)
        fun onTaskFailed(message: String)
    }
}