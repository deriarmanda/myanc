package um.teknik.informatika.freelancer.myanc.feature.splashscreen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_splash_screen.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.feature.home.HomeActivity
import um.teknik.informatika.freelancer.myanc.feature.login.LoginActivity

class SplashScreenActivity : AppCompatActivity(), SplashScreenContract.View {

    override lateinit var presenter: SplashScreenContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        presenter = SplashScreenPresenter(
                this,
                FirebaseAuth.getInstance(),
                AccountRepository.getInstance(this)
        )
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onUserAlreadyLogin() {
        Toast.makeText(this, R.string.login_msg_auth_success, Toast.LENGTH_LONG).show()
        startActivity(HomeActivity.getIntent(this))
        finish()
    }

    override fun onAnonymousLoginSuccess() {
        startActivity(LoginActivity.getIntent(this))
        finish()
    }

    override fun onAnonymousLoginFailed(msgRes: Int) {
        progress_bar.visibility = View.GONE
        text_msg.visibility = View.VISIBLE
        text_msg.setText(msgRes)
    }

    override fun setLoadingIndicator(active: Boolean) {
        text_msg.visibility = if (active) View.VISIBLE else View.GONE
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }
}
