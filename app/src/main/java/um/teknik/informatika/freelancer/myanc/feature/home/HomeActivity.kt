package um.teknik.informatika.freelancer.myanc.feature.home

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Identitas
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.User
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.IdentitasRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KehamilanRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository
import um.teknik.informatika.freelancer.myanc.feature.anc.detail.DetailAncActivity
import um.teknik.informatika.freelancer.myanc.feature.identitas.IdentitasPasienActivity
import um.teknik.informatika.freelancer.myanc.feature.kehamilan.ListKehamilanActivity
import um.teknik.informatika.freelancer.myanc.feature.login.LoginActivity
import um.teknik.informatika.freelancer.myanc.util.DateUtils

class HomeActivity : AppCompatActivity(), HomeContract.View {

    override lateinit var presenter: HomeContract.Presenter
    private lateinit var snackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        presenter = HomePresenter(
                this,
                AccountRepository.getInstance(this),
                IdentitasRepository.getInstance(),
                KehamilanRepository.getInstance(),
                KunjunganRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                snackbar.dismiss()
                presenter.start()
            }
        }
        button_more_profile.setOnClickListener {
            presenter.openIdentitasPasienPage()
        }
        button_detail_anc.setOnClickListener {
            presenter.openDetailAncPage()
        }
        button_show_record.setOnClickListener {
            startActivity(ListKehamilanActivity.getIntent(this))
        }
        button_call_petugas.setOnClickListener {
            presenter.callPetugas()
        }
        button_exit.setOnClickListener {
            presenter.doSignOut()
        }
        snackbar = Snackbar.make(
                refresh_layout,
                R.string.general_error_no_internet,
                Snackbar.LENGTH_INDEFINITE
        )
        snackbar.setAction(R.string.general_action_retry) {
            presenter.start()
        }

        presenter.start()
    }

    override fun onLoadIdentitasUserSucceed(identitas: Identitas) {
        // Ibu
        text_nama.text = identitas.namaIbu
        text_ttl.text = identitas.ttlIbu
        text_alamat.text = identitas.alamat
        text_kehamilan_ke.text = String.format(
                "Kehamilan ke - %d",
                identitas.kehamilanKe
        )

        // Petugas
        text_nama_petugas.text = identitas.namaPetugas
        text_telepon_petugas.text = identitas.telpPetugas
    }

    override fun onLoadAkunUserSucceed(user: User) {
        form_norm.text = user.noRM
        form_password.text = user.password
    }

    override fun onLoadCatatanTerkiniSucceed(anc: Kunjungan) {
        text_tanggal_anc.text = DateUtils.convertDate(anc.tanggal)
        text_kehamilan_anc.text = getString(R.string.detail_hint_kehamilan, anc.umurKehamilan)
        text_others_anc.text = String.format(
                "%s - %s",
                anc.tekananDarah,
                anc.beratBadan
        )
        text_tgl_kunjungan.text = DateUtils.convertDate(anc.tanggalKembali)
    }

    override fun onLoadAllDataFailed(msgRes: Int) {
        snackbar.setText(msgRes).show()
    }

    override fun onSignedOut() {
        startActivity(LoginActivity.getIntent(this))
        finish()
    }

    override fun showDialerPage(phone: Uri) {
        startActivity(Intent(Intent.ACTION_DIAL, phone))
    }

    override fun showIdentitasPasienPage(identitas: Identitas) {
        startActivity(IdentitasPasienActivity.getIntent(this, identitas))
    }

    override fun showDetailAncPage(anc: Kunjungan) {
        startActivity(DetailAncActivity.getIntent(this, anc))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
