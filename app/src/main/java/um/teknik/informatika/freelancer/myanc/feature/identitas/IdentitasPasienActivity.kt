package um.teknik.informatika.freelancer.myanc.feature.identitas

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_identitas_pasien.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Identitas
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.IdentitasRepository

class IdentitasPasienActivity : AppCompatActivity(), IdentitasPasienContract.View {

    override lateinit var presenter: IdentitasPasienContract.Presenter

    companion object {
        private const val EXTRA_IDENTITAS = "extra_identitas_pasien"
        fun getIntent(context: Context, identitas: Identitas): Intent {
            val intent = Intent(context, IdentitasPasienActivity::class.java)
            intent.putExtra(EXTRA_IDENTITAS, identitas)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_identitas_pasien)

        presenter = IdentitasPasienPresenter(
                this,
                IdentitasRepository.getInstance(),
                AccountRepository.getInstance(this)
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener { presenter.reloadIdentitas() }
        }
        button_call_petugas.setOnClickListener {
            startActivity(Intent(
                    Intent.ACTION_DIAL,
                    Uri.parse("tel: ${text_telepon_petugas.text}")
            ))
        }

        val identitas = intent.getParcelableExtra<Identitas>(EXTRA_IDENTITAS)
        if (identitas == null) presenter.reloadIdentitas()
        else fetchData(identitas)
    }

    override fun showIdentitas(identitas: Identitas) {
        fetchData(identitas)
    }

    override fun showErrorDialog(msgRes: Int) {
        Snackbar.make(refresh_layout, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.general_action_retry) {
                    presenter.reloadIdentitas()
                }
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }

    private fun fetchData(identitas: Identitas) {
        // Info Administrasi
        text_no_registrasi.text = String.format(
                "%s / %s",
                identitas.noRegistrasi,
                identitas.noUrut
        )
        text_tgl_terima.text = identitas.tglTerima
        text_nama_petugas.text = identitas.namaPetugas
        text_telepon_petugas.text = identitas.telepon

        // Identitas Ibu
        text_nama_ibu.text = identitas.namaIbu
        text_ttl_ibu.text = identitas.ttlIbu
        text_kehamilan.text = identitas.kehamilanKe.toString()
        text_umur_anak.text = identitas.umurAnakTerakhir
        text_agama_ibu.text = identitas.agamaIbu
        text_pendidikan_ibu.text = identitas.pendidikanIbu
        text_goldarah_ibu.text = identitas.golDarahIbu
        text_pekerjaan_ibu.text = identitas.pekerjaanIbu
        text_no_jkn.text = identitas.noJkn

        // Identitas Suami
        text_nama_suami.text = identitas.namaSuami
        text_ttl_suami.text = identitas.ttlSuami
        text_agama_suami.text = identitas.agamaSuami
        text_pendidikan_suami.text = identitas.pendidikanSuami
        text_goldarah_suami.text = identitas.golDarahSuami
        text_pekerjaan_suami.text = identitas.pekerjaanSuami

        // Informasi Lain
        text_alamat.text = identitas.alamat
        text_kecamatan.text = identitas.alamat
        text_kota.text = identitas.kota
        text_telepon.text = identitas.telepon
    }
}
