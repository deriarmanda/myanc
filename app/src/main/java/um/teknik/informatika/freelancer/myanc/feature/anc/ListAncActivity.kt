package um.teknik.informatika.freelancer.myanc.feature.anc

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_list_anc.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository
import um.teknik.informatika.freelancer.myanc.feature.anc.detail.DetailAncActivity

class ListAncActivity : AppCompatActivity(), ListAncContract.View {

    override lateinit var presenter: ListAncPresenter
    private lateinit var snackBar: Snackbar
    private val mItemClickListener =
            object : ListAncRvAdapter.OnItemClickListener {
                override fun onItemClick(anc: Kunjungan) {
                    startActivity(DetailAncActivity.getIntent(
                            this@ListAncActivity,
                            anc
                    ))
                }
            }
    private val mAdapter = ListAncRvAdapter(mItemClickListener)

    companion object {
        private const val EXTRA_KEHAMILAN = "extra_data_kehamilan"
        fun getIntent(context: Context, kehamilan: Kehamilan): Intent {
            val intent = Intent(context, ListAncActivity::class.java)
            intent.putExtra(EXTRA_KEHAMILAN, kehamilan)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_anc)

        val kehamilan = intent.getParcelableExtra<Kehamilan>(EXTRA_KEHAMILAN)
        presenter = ListAncPresenter(
                this,
                kehamilan,
                KunjunganRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(
                    R.color.accent,
                    R.color.primary
            )
            setOnRefreshListener {
                snackBar.dismiss()
                presenter.loadListAnc()
            }
        }
        with(recycler_view) {
            val linearLayout = LinearLayoutManager(this@ListAncActivity)
            linearLayout.reverseLayout = true
            linearLayout.stackFromEnd = true
            layoutManager = linearLayout
            adapter = mAdapter
        }
        snackBar = Snackbar.make(
                refresh_layout,
                R.string.general_error_no_internet,
                Snackbar.LENGTH_INDEFINITE
        )
        snackBar.setAction(R.string.general_action_retry) {
            presenter.loadListAnc()
        }

        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showListAnc(list: List<Kunjungan>) {
        text_empty_msg.visibility = View.GONE
        recycler_view.visibility = View.VISIBLE
        mAdapter.listAnc = list
    }

    override fun showEmptyListMessage() {
        text_empty_msg.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
    }

    override fun showErrorMessage(msgRes: Int) {
        text_empty_msg.visibility = View.GONE
        recycler_view.visibility = View.GONE
        snackBar.setText(msgRes).show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
