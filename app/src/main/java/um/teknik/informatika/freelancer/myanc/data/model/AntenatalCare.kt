package um.teknik.informatika.freelancer.myanc.data.model

data class AntenatalCare(
        val kehamilanKe: String = "",
        val jmlPersalinan: String = "",
        val jmlKeguguran: String = "",
        val g: String = "",
        val p: String = "",
        val a: String = "",
        val jmlAnakHidup: String = "",
        val jmlLahirMati: String = "",
        val jmlKurangBulan: String = "",
        val lingkarLengan: String = "",
        val kek: String = "",
        val nonKek: String = "",
        val tinggiBadan: String = "",
        val golDarah: String = "",
        val hpht: String = "",
        val htp: String = "",
        val kontrasepsi: String = "",
        val jarakKehamilan: String = "",
        val imunisasi: String = "",
        val penolong: String = "",
        val persalinanTerakhir: String = "",
        val tindakan: String = "",
        val penyakit: String = "",
        val alergi: String = ""
)