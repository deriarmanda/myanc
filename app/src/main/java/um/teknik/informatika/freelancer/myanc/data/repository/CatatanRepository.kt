package um.teknik.informatika.freelancer.myanc.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.freelancer.myanc.data.model.AntenatalCare
import um.teknik.informatika.freelancer.myanc.data.model.Skrining

class CatatanRepository private constructor() {

    private val mDatabase = FirebaseDatabase.getInstance()

    companion object {
        private var sIntance: CatatanRepository? = null
        fun getInstance(): CatatanRepository {
            if (sIntance == null) sIntance = CatatanRepository()
            return sIntance!!
        }
    }

    fun getAnc(uidAnc: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val anc = data.getValue(AntenatalCare::class.java)
                    if (anc != null) callback.onGetAncSuccess(anc)
                    else callback.onTaskFailed("NPE occured while trying to parse Data ANC 1 value from Firebase")
                } else callback.onGetAncSuccess(AntenatalCare())
            }
        }

        val ancRef = mDatabase.getReference("anc_1").child(uidAnc)
        ancRef.addListenerForSingleValueEvent(listener)
    }

    fun getSkrining(uidSkrining: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val list = arrayListOf<Skrining>()
                    for (child in data.children) {
                        val skrining = child.getValue(Skrining::class.java)
                        if (skrining == null) {
                            callback.onTaskFailed("NPE occured while trying to parse Data Skrining value from Firebase")
                            return
                        } else list.add(skrining)
                    }
                    callback.onGetSkriningSuccess(list)
                } else callback.onGetSkriningSuccess(emptyList())
            }
        }

        val skriningRef =
                mDatabase.getReference("skrining").child(uidSkrining)
        skriningRef.addListenerForSingleValueEvent(listener)
    }

    interface Callback {
        fun onGetAncSuccess(anc: AntenatalCare)
        fun onGetSkriningSuccess(list: List<Skrining>)
        fun onTaskFailed(message: String)
    }
}