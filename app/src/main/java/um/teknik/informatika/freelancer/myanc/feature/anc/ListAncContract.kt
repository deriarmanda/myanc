package um.teknik.informatika.freelancer.myanc.feature.anc

import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan

interface ListAncContract {

    interface Presenter : BasePresenter {
        fun loadListAnc()
    }

    interface View : BaseView<Presenter> {
        fun showListAnc(list: List<Kunjungan>)
        fun showEmptyListMessage()
        fun showErrorMessage(@StringRes msgRes: Int)
    }
}