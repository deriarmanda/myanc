package um.teknik.informatika.freelancer.myanc.util

import android.content.Context
import um.teknik.informatika.freelancer.myanc.data.model.User

const val SHARED_PREF_NAME = "myanc_pref"
const val PREF_KEY_USER_UID = "user_uid"
const val PREF_KEY_USER_NO_RM = "user_no_rm"
const val PREF_KEY_USER_PASSWORD = "user_password"

class PreferenceManager private constructor(val context: Context) {

    private val sharedPref = context.getSharedPreferences(SHARED_PREF_NAME, 0)

    companion object {
        fun getInstance(context: Context) = PreferenceManager(context)
    }

    fun isCachedUserExists() = !sharedPref.getString(PREF_KEY_USER_UID, "-").equals("-")

    fun getCachedUser(): User {
        return User(
                sharedPref.getString(PREF_KEY_USER_UID, "-"),
                sharedPref.getString(PREF_KEY_USER_NO_RM, "-"),
                sharedPref.getString(PREF_KEY_USER_PASSWORD, "-")
        )
    }

    fun saveCurrentUser(user: User) {
        val editor = sharedPref.edit()
        editor.putString(PREF_KEY_USER_UID, user.uid)
        editor.putString(PREF_KEY_USER_NO_RM, user.noRM)
        editor.putString(PREF_KEY_USER_PASSWORD, user.password)
        editor.apply()
    }

    fun clearCachedUser() {
        val editor = sharedPref.edit()
        editor.remove(PREF_KEY_USER_UID)
        editor.remove(PREF_KEY_USER_NO_RM)
        editor.remove(PREF_KEY_USER_PASSWORD)
        editor.apply()
    }
}