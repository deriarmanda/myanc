package um.teknik.informatika.freelancer.myanc.base

interface BaseView<T> {

    val presenter: T
    fun setLoadingIndicator(active: Boolean)
}