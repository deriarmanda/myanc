package um.teknik.informatika.freelancer.myanc.data.model

import android.os.Parcel
import android.os.Parcelable

data class Identitas(
        val noRegistrasi: String = "",
        val noUrut: String = "",
        val tglTerima: String = "",
        val namaPetugas: String = "",
        val telpPetugas: String = "",
        val namaIbu: String = "",
        val ttlIbu: String = "",
        val kehamilanKe: Int = 1,
        val umurAnakTerakhir: String = "",
        val agamaIbu: String = "",
        val pendidikanIbu: String = "",
        val golDarahIbu: String = "",
        val pekerjaanIbu: String = "",
        val noJkn: String = "",
        val namaSuami: String = "",
        val ttlSuami: String = "",
        val agamaSuami: String = "",
        val pendidikanSuami: String = "",
        val golDarahSuami: String = "",
        val pekerjaanSuami: String = "",
        val alamat: String = "",
        val kecamatan: String = "",
        val kota: String = "",
        val telepon: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(noRegistrasi)
        parcel.writeString(noUrut)
        parcel.writeString(tglTerima)
        parcel.writeString(namaPetugas)
        parcel.writeString(telpPetugas)
        parcel.writeString(namaIbu)
        parcel.writeString(ttlIbu)
        parcel.writeInt(kehamilanKe)
        parcel.writeString(umurAnakTerakhir)
        parcel.writeString(agamaIbu)
        parcel.writeString(pendidikanIbu)
        parcel.writeString(golDarahIbu)
        parcel.writeString(pekerjaanIbu)
        parcel.writeString(noJkn)
        parcel.writeString(namaSuami)
        parcel.writeString(ttlSuami)
        parcel.writeString(agamaSuami)
        parcel.writeString(pendidikanSuami)
        parcel.writeString(golDarahSuami)
        parcel.writeString(pekerjaanSuami)
        parcel.writeString(alamat)
        parcel.writeString(kecamatan)
        parcel.writeString(kota)
        parcel.writeString(telepon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Identitas> {
        override fun createFromParcel(parcel: Parcel): Identitas {
            return Identitas(parcel)
        }

        override fun newArray(size: Int): Array<Identitas?> {
            return arrayOfNulls(size)
        }
    }
}