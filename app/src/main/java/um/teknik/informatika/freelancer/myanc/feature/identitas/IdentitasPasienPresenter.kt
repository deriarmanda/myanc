package um.teknik.informatika.freelancer.myanc.feature.identitas

import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Identitas
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.IdentitasRepository

class IdentitasPasienPresenter(
        private val mView: IdentitasPasienContract.View,
        private val mIdentitasRepo: IdentitasRepository,
        private val mAccountRepo: AccountRepository
) : IdentitasPasienContract.Presenter {

    override fun reloadIdentitas() {
        mView.setLoadingIndicator(true)
        val user = mAccountRepo.getCurrentUser()

        mIdentitasRepo.getIdentitas(
                user.uid,
                object : IdentitasRepository.Callback {
                    override fun onGetIdentitasSucceed(identitas: Identitas) {
                        mView.setLoadingIndicator(false)
                        mView.showIdentitas(identitas)
                    }

                    override fun onGetIdentitasFailed(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.showErrorDialog(R.string.general_error_no_internet)
                    }
                }
        )
    }

    override fun start() {}
}