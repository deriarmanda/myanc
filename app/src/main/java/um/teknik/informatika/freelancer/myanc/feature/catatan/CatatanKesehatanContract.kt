package um.teknik.informatika.freelancer.myanc.feature.catatan

import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView
import um.teknik.informatika.freelancer.myanc.data.model.AntenatalCare
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.Skrining

interface CatatanKesehatanContract {

    interface Presenter : BasePresenter {
        fun loadStaticAnc()
        fun loadListSkrining()
        fun loadKunjunganAnc(limit: Int)
    }

    interface View : BaseView<Presenter> {
        fun showStaticAnc(anc: AntenatalCare)
        fun showListSkrining(list: List<Skrining>)
        fun showListKunjunganAnc(list: List<Kunjungan>)
        fun onLoadDataFailed(@StringRes msgRes: Int)
        fun openFullListKunjunganAnc()
        fun openDetailKunjunganAnc(anc: Kunjungan)
    }
}