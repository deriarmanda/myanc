package um.teknik.informatika.freelancer.myanc.base

interface BaseRepositoryCallback {
    fun onRepositoryTaskSuccess()
    fun onRepositoryTaskFailed(message: String)
}