package um.teknik.informatika.freelancer.myanc.feature.home

import android.net.Uri
import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView
import um.teknik.informatika.freelancer.myanc.data.model.Identitas
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.User

interface HomeContract {

    interface Presenter : BasePresenter {
        fun loadAkunUser()
        fun loadIdentitasUser(user: User)
        fun loadCatatanTerkini(user: User)
        fun doSignOut()
        fun callPetugas()
        fun openIdentitasPasienPage()
        fun openDetailAncPage()
    }

    interface View : BaseView<Presenter> {
        fun onLoadIdentitasUserSucceed(identitas: Identitas)
        fun onLoadAkunUserSucceed(user: User)
        fun onLoadCatatanTerkiniSucceed(anc: Kunjungan)
        fun onLoadAllDataFailed(@StringRes msgRes: Int)
        fun onSignedOut()
        fun showDialerPage(phone: Uri)
        fun showIdentitasPasienPage(identitas: Identitas)
        fun showDetailAncPage(anc: Kunjungan)
    }
}