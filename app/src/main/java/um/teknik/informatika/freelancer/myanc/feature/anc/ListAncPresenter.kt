package um.teknik.informatika.freelancer.myanc.feature.anc

import android.util.Log
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository

class ListAncPresenter(
        private val mView: ListAncContract.View,
        private val mKehamilan: Kehamilan,
        private val kunjunganRepo: KunjunganRepository
) : ListAncContract.Presenter {

    override fun loadListAnc() {
        mView.setLoadingIndicator(true)
        val uidAnc = mKehamilan.uidAnc

        kunjunganRepo.getListKunjungan(
                uidAnc,
                object : KunjunganRepository.Callback {
                    override fun onGetCurrentKunjunganSuccess(anc: Kunjungan) {}
                    override fun onGetListKunjunganSuccess(list: List<Kunjungan>) {
                        mView.setLoadingIndicator(false)
                        if (list.isEmpty()) mView.showEmptyListMessage()
                        else mView.showListAnc(list)
                    }

                    override fun onTaskFailed(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.showErrorMessage(R.string.general_error_no_internet)
                        Log.d("ListAncPresenter", message)
                    }
                }
        )
    }

    override fun start() = loadListAnc()
}