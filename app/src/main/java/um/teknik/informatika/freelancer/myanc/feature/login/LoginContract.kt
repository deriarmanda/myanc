package um.teknik.informatika.freelancer.myanc.feature.login

import android.support.annotation.StringRes
import um.teknik.informatika.freelancer.myanc.base.BasePresenter
import um.teknik.informatika.freelancer.myanc.base.BaseView

interface LoginContract {

    interface Presenter : BasePresenter {
        fun doLogin(noRM: String, password: String)
    }

    interface View : BaseView<Presenter> {
        fun onAuthenticationSuccess()
        fun onAuthenticationFailed(@StringRes msgRes: Int)
        fun onNetworkErrorOccured()
    }
}