package um.teknik.informatika.freelancer.myanc.data.repository

import android.content.Context
import android.support.annotation.StringRes
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.base.BaseRepositoryCallback
import um.teknik.informatika.freelancer.myanc.data.model.User
import um.teknik.informatika.freelancer.myanc.util.PreferenceManager

class AccountRepository private constructor(context: Context) {

    private var mUser: User? = null
    private val mPrefManager = PreferenceManager.getInstance(context)
    private val mDatabase = FirebaseDatabase.getInstance().getReference("/users")

    companion object {
        private var sInstance: AccountRepository? = null
        fun getInstance(context: Context): AccountRepository {
            if (sInstance == null) sInstance = AccountRepository(context)
            return sInstance!!
        }
    }

    fun getCachedUser(): User? {
        mUser = if (mPrefManager.isCachedUserExists()) mPrefManager.getCachedUser()
                else null
        return mUser
    }

    fun getCurrentUser() = mUser!!

    fun authenticate(noRM: String, password: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onRepositoryTaskFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                Log.d("AccountRepository", "${data.exists()} : ${data.key} - ${data.value}")
                for (user in data.children) {
                    val currNoRM = user.child("no_rm").value.toString()
                    val currPass = user.child("password").value.toString()
                    mUser = null
                    if (currNoRM == noRM && currPass == password) {
                        mUser = User(
                                user.key.toString(),
                                currNoRM,
                                currPass
                        )
                        break
                    }
                    val currUser = user.getValue(User::class.java)
                    Log.d("AccountRepository", "${currUser?.uid} - ${currUser?.noRM}")
                    Log.d("AccountRepository", "Key: ${user.key}")
                    Log.d("AccountRepository", "Value: ${user.value.toString()}")
                    Log.d("AccountRepository", "Child no_rm: ${user.child("no_rm").value}")
                }

                if (mUser != null) {
                    callback.onAuthenticateSuccess()
                    cachedCurrentUser()
                }
                else callback.onAuthenticateFailed(R.string.login_error_credential)
            }
        }
        mDatabase.addListenerForSingleValueEvent(listener)
    }

    fun logoutUser(callback: BaseRepositoryCallback) {
        mPrefManager.clearCachedUser()
        mUser = null
        callback.onRepositoryTaskSuccess()
    }

    private fun cachedCurrentUser() {
        if (mUser == null) return
        mPrefManager.saveCurrentUser(mUser!!)
    }

    interface Callback : BaseRepositoryCallback {
        fun onAuthenticateSuccess()
        fun onAuthenticateFailed(@StringRes msgRes: Int)
    }
}