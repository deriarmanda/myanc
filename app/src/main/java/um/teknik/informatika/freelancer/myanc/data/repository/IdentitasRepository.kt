package um.teknik.informatika.freelancer.myanc.data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.teknik.informatika.freelancer.myanc.data.model.Identitas

class IdentitasRepository private constructor() {

    private var mIdentitas: Identitas? = null
    private val mDbReference = FirebaseDatabase
            .getInstance().getReference("/identitas")

    companion object {
        private var sInstance: IdentitasRepository? = null
        fun getInstance(): IdentitasRepository {
            if (sInstance == null) sInstance = IdentitasRepository()
            return sInstance!!
        }
    }

    fun getIdentitas(uidUser: String, callback: Callback) {
        if (mIdentitas == null) reloadIdentitas(uidUser, callback)
        else callback.onGetIdentitasSucceed(mIdentitas!!)
    }

    private fun reloadIdentitas(uidUser: String, callback: Callback) {
        val listener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onGetIdentitasFailed(dbError.message)
            }

            override fun onDataChange(data: DataSnapshot) {
                if (data.exists()) {
                    val identitas = data.getValue(Identitas::class.java)
                    if (identitas != null) callback.onGetIdentitasSucceed(identitas)
                    else callback.onGetIdentitasFailed("NPE occured while trying to parse Identitas value from Firebase")
                } else callback.onGetIdentitasSucceed(Identitas())
            }
        }

        val userRef = mDbReference.child(uidUser)
        userRef.addListenerForSingleValueEvent(listener)
    }

    interface Callback {
        fun onGetIdentitasSucceed(identitas: Identitas)
        fun onGetIdentitasFailed(message: String)
    }
}