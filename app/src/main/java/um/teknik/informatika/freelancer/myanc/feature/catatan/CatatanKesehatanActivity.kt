package um.teknik.informatika.freelancer.myanc.feature.catatan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_catatan_kesehatan.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.AntenatalCare
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.model.Kunjungan
import um.teknik.informatika.freelancer.myanc.data.model.Skrining
import um.teknik.informatika.freelancer.myanc.data.repository.CatatanRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KunjunganRepository
import um.teknik.informatika.freelancer.myanc.feature.anc.ListAncActivity
import um.teknik.informatika.freelancer.myanc.feature.anc.detail.DetailAncActivity

class CatatanKesehatanActivity : AppCompatActivity(), CatatanKesehatanContract.View {

    override lateinit var presenter: CatatanKesehatanPresenter
    private lateinit var mKehamilan: Kehamilan
    private val kunjunganAncClickListener =
            object : HorizontalAncRvAdapter.OnItemClickListener {
                override fun onItemClick(anc: Kunjungan) {
                    openDetailKunjunganAnc(anc)
                }
            }
    private val mKunjunganAdapter = HorizontalAncRvAdapter(kunjunganAncClickListener)
    private val mSkriningAdapter = SkriningRvAdapter()

    companion object {
        private const val EXTRA_KEHAMILAN = "extra_data_kehamilan"
        fun getIntent(context: Context, kehamilan: Kehamilan): Intent {
            val intent = Intent(context, CatatanKesehatanActivity::class.java)
            intent.putExtra(EXTRA_KEHAMILAN, kehamilan)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catatan_kesehatan)

        mKehamilan = intent.getParcelableExtra(EXTRA_KEHAMILAN)
        presenter = CatatanKesehatanPresenter(
                this,
                mKehamilan,
                CatatanRepository.getInstance(),
                KunjunganRepository.getInstance()
        )

        with(rv_skrining) {
            layoutManager = LinearLayoutManager(this@CatatanKesehatanActivity)
            adapter = mSkriningAdapter
        }
        with(rv_kunjungan) {
            layoutManager = LinearLayoutManager(
                    this@CatatanKesehatanActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
            )
            adapter = mKunjunganAdapter
        }
        button_more_kunjungan.setOnClickListener { openFullListKunjunganAnc() }
        fab_show_record.setOnClickListener { openFullListKunjunganAnc() }

        presenter.start()
    }

    override fun showStaticAnc(anc: AntenatalCare) {
        fetchData(anc)
    }

    override fun showListSkrining(list: List<Skrining>) {
        mSkriningAdapter.listSkrining = list
    }

    override fun showListKunjunganAnc(list: List<Kunjungan>) {
        mKunjunganAdapter.listAnc = list
    }

    override fun onLoadDataFailed(msgRes: Int) {
        Snackbar.make(fab_show_record, msgRes, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.general_action_retry) {
                    presenter.start()
                }
    }

    override fun openFullListKunjunganAnc() {
        startActivity(ListAncActivity.getIntent(this, mKehamilan))
    }

    override fun openDetailKunjunganAnc(anc: Kunjungan) {
        startActivity(DetailAncActivity.getIntent(this, anc))
    }

    override fun setLoadingIndicator(active: Boolean) {
        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
    }

    private fun fetchData(anc: AntenatalCare) {
        text_kehamilan.text = anc.kehamilanKe
        text_persalinan.text = anc.jmlPersalinan
        text_keguguran.text = anc.jmlKeguguran
        text_g.text = anc.g
        text_p.text = anc.p
        text_a.text = anc.a
        text_anak_hidup.text = anc.jmlAnakHidup
        text_lahir_mati.text = anc.jmlLahirMati
        text_lahir_kurang.text = anc.jmlKurangBulan
        text_lingkar_lengan.text = anc.lingkarLengan
        text_kek.text = anc.kek
        text_non_kek.text = anc.nonKek
        text_tinggi_badan.text = anc.tinggiBadan
        text_goldarah.text = anc.golDarah

        text_hpht.text = anc.hpht
        text_htp.text = anc.htp
        text_kontrasepsi.text = anc.kontrasepsi
        text_jarak_kehamilan.text = anc.jarakKehamilan
        text_imunisasi.text = anc.imunisasi
        text_penolong.text = anc.penolong
        text_persalinan_terakhir.text = anc.persalinanTerakhir
        text_tindakan.text = anc.tindakan
        text_penyakit.text = anc.penyakit
        text_alergi.text = anc.alergi
    }
}
