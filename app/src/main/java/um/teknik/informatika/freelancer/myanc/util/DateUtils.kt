package um.teknik.informatika.freelancer.myanc.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    private const val DATE_PATTERN = "yyyy-MM-dd"
    private const val HUMAN_DATE_PATTERN = "d MMMM yyyy"
    private val DATE_FORMATTER = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
    private val HUMAN_DATE_FORMATTER = SimpleDateFormat(HUMAN_DATE_PATTERN, Locale.getDefault())

    fun parseDate(dateString: String) = if (dateString.isBlank()) Date() else DATE_FORMATTER.parse(dateString)
    fun formatDate(date: Date) = DATE_FORMATTER.format(date)
    fun convertDate(dateString: String): String {
        val converted =
                if (dateString.contains("/")) dateString.replace("/", "-")
                else dateString
        val date = parseDate(converted)
        return HUMAN_DATE_FORMATTER.format(date)
    }
}