package um.teknik.informatika.freelancer.myanc.feature.catatan

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_skrining.view.*
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Skrining

class SkriningRvAdapter : RecyclerView.Adapter<SkriningRvAdapter.ViewHolder>() {

    var listSkrining = emptyList<Skrining>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_skrining, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = listSkrining.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(listSkrining[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(skrining: Skrining) {
            itemView.text_hint.text = skrining.key
            itemView.text_value.text = skrining.value
        }
    }
}