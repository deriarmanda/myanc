package um.teknik.informatika.freelancer.myanc.feature.splashscreen

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository

class SplashScreenPresenter(
        private val view: SplashScreenContract.View,
        private val auth: FirebaseAuth,
        private val accountRepo: AccountRepository
) : SplashScreenContract.Presenter {

    override fun checkAnonymousFirebaseUser() {
        view.setLoadingIndicator(true)
        val user = auth.currentUser

        if (user == null) loginAnonymouslyToFirebase()
        else checkLogedInUser()
    }

    override fun checkLogedInUser() {
        view.setLoadingIndicator(true)
        val user = accountRepo.getCachedUser()

        if (user == null) view.onAnonymousLoginSuccess()
        else view.onUserAlreadyLogin()
    }

    override fun loginAnonymouslyToFirebase() {
        view.setLoadingIndicator(true)
        auth.signInAnonymously().addOnCompleteListener {
            view.setLoadingIndicator(false)
            Log.d("SplashScreenPresenter", "Success: ${it.isSuccessful} - ${auth.currentUser?.uid}")
            if (it.isSuccessful) checkLogedInUser()
            else view.onAnonymousLoginFailed(R.string.splash_error_no_internet)
        }
    }

    override fun start() = checkAnonymousFirebaseUser()
}