package um.teknik.informatika.freelancer.myanc.base

interface BasePresenter {
    fun start()
}