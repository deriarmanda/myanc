package um.teknik.informatika.freelancer.myanc.feature.kehamilan

import android.util.Log
import um.teknik.informatika.freelancer.myanc.R
import um.teknik.informatika.freelancer.myanc.data.model.Kehamilan
import um.teknik.informatika.freelancer.myanc.data.repository.AccountRepository
import um.teknik.informatika.freelancer.myanc.data.repository.KehamilanRepository

class ListKehamilanPresenter(
        private val view: ListKehamilanContract.View,
        private val kehamilanRepo: KehamilanRepository,
        private val accountRepo: AccountRepository
) : ListKehamilanContract.Presenter {

    override fun loadListKehamilan() {
        view.setLoadingIndicator(true)
        val user = accountRepo.getCurrentUser()
        kehamilanRepo.getListKehamilan(
                user.uid,
                object : KehamilanRepository.Callback {
                    override fun onGetCurrentKehamilanSuccess(kehamilan: Kehamilan) {}
                    override fun onLoadListKehamilanSuccess(list: List<Kehamilan>) {
                        view.setLoadingIndicator(false)
                        if (list.isEmpty()) view.showEmptyListMessage()
                        else view.onLoadListKehamilanSucceed(list)
                    }

                    override fun onTaskFailed(message: String) {
                        view.setLoadingIndicator(false)
                        view.onLoadListKehamilanFailed(R.string.general_error_no_internet)
                        Log.d("ListKehamilanPresenter", message)
                    }
                }
        )
    }

    override fun start() = loadListKehamilan()
}